CREATE TABLE `libtimer_log` (
	`id` int(11) unsigned NOT NULL auto_increment,
	`module` varchar(255) NOT NULL default '',
	`description` text NOT NULL,
	`created` datetime NOT NULL default '0000-00-00 00:00:00',
	`event` varchar(32) NOT NULL default '',
	PRIMARY KEY (`id`),
	KEY `module` (`module`),
	KEY `created` (`created`)
);